defmodule Rollixir.Repo.Migrations.CreateDices do
  use Ecto.Migration

  def change do
    create table(:dices) do
      add :value,  :integer
      add :result, :integer

      timestamps
    end
  end
end
