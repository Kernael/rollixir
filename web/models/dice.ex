defmodule Rollixir.Dice do
  use Ecto.Model

  schema "dices" do
    field :value,  :integer
    field :result, :integer

    timestamps
  end

  @required_field ~w(value result)
  @optional_field ~w()

  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_field, @optional_field)
    |> validate_number(:value, greater_than: 0)
    |> validate_number(:result, greater_than: 0)
  end
end
