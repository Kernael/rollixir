defmodule Rollixir.Router do
  use Rollixir.Web, :router

  pipeline :browser do
    plug :accepts, ~w(html)
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
  end

  pipeline :api do
    plug :accepts, ~w(json)
  end

  scope "/", Rollixir do
    pipe_through :browser

    get "/", PageController, :index
  end

  scope "/api", Rollixir do
    pipe_through :api

    resources "/dices", DiceController, except: ~w(new edit)a
  end
end
