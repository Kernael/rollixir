defmodule Rollixir.DiceController do
  use Rollixir.Web, :controller

  alias Rollixir.Repo
  alias Rollixir.Dice

  plug :action

  def index(conn, _params) do
    dices = Repo.all(Dice)

    render conn, :index, dices: dices
  end

  def show(conn, %{"id" => id}) do
    dice = Repo.get(Dice, id)

    render conn, :show, dice: dice
  end

  def create(conn, %{"value" => value} = params) do
    changeset = Dice.changeset(%Dice{}, params)

    if changeset.valid? do
      dice = Repo.insert(changeset)
      render conn, :create, dice: dice
    else
      conn
      |> put_status(:unprocessable_entity)
      |> render Rollixir.ChangesetView, :error, changeset: changeset
    end
  end
end
