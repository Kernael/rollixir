defmodule Rollixir.DiceView do
  use Rollixir.Web, :view

  def render("index.json", %{dices: dices}) do
    dices
  end

  def render("show.json", %{dice: dice}) do
    dice
  end

  def render("create.json", %{dice: dice}) do
    dice
  end
end
