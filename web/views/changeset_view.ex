defmodule Rollixir.ChangesetView do
  use Rollixir.Web, :view

  def render("error.json", %{changeset: changeset}) do
    changeset
  end
end
